import { Injectable } from '@angular/core';
import { addDoc, collection, collectionData, deleteDoc, doc, Firestore, getDoc, getDocs, query, updateDoc } from '@angular/fire/firestore';
import { where } from '@firebase/firestore';
import { Observable } from 'rxjs';
import { Player } from '../commons/interfaces/player.interface';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor(private firestore: Firestore) { }

  addPlayer(player: Player) {
    const playerRef = collection(this.firestore, 'players');
    return addDoc(playerRef, player);
  }
  getPlayers(filter = '') {
    const playersRef = collection(this.firestore, 'players');
    let q = query(playersRef);
    if (filter) {
      q = query(playersRef, where('name', '==', filter));
    }
    return collectionData(q) as unknown as Observable<Player[]>;
  }
  async updatePlayer(player: Player) {
    const playersRef = collection(this.firestore, 'players');
    let q = query(playersRef, where('id', '==', player.id));
    const querySnapshot = await getDocs(q);

    querySnapshot.forEach(async document => {
      const docRef = doc(this.firestore, 'players', document.id);
      await updateDoc(docRef, { ...player });
    })
  }

  async deletePlayer(id: string) {
    const playersRef = collection(this.firestore, 'players');
    let q = query(playersRef, where('id', '==', id));
    const querySnapshot = await getDocs(q);

    querySnapshot.forEach(async document => {
      const docRef = doc(this.firestore, 'players', document.id);
      await deleteDoc(docRef);
    })
  }
}
