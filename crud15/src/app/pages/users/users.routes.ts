import { Routes } from "@angular/router";
import { UserAddComponent } from "./user-add/user-add.component";
import { UserEditComponent } from "./user-edit/user-edit.component";
import { UserListComponent } from "./user-list/user-list.component";

export const UserRoutes: Routes = [
  { path: '', title: 'Lista de Jugadores', component: UserListComponent },
  { path: 'add', title: 'Agregar Nuevos Jugadores', component: UserAddComponent },
  { path: 'edit', title: 'Editar Jugador', component: UserEditComponent },
]
